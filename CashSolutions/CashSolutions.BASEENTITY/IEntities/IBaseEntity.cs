﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BuildingBlocks.BaseEntities.IEntities
{
    public interface IBaseEntity
    {

        string CreatedBy_Id { get; set; }

        string UpdatedBy_Id { get; set; }

        string DeletedBy_Id { get; set; }

        string Tenant_Id { set; get; }

        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
        DateTime DeletedDate { get; set; }
    }
}
