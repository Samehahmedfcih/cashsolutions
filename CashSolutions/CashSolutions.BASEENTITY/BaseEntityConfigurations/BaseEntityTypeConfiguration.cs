﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CashSolutions.BuildingBlocks.BaseEntities.EntityConfigurations
{
    public class BaseEntityTypeConfiguration<T>
        : IEntityTypeConfiguration<T> where T : class, IBaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> BaseEntityConfiguration)
        {
            BaseEntityConfiguration.Property(o => o.IsDeleted).IsRequired();
            BaseEntityConfiguration.Property(o => o.Tenant_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.CreatedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.UpdatedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.DeletedBy_Id).IsRequired(false);
            BaseEntityConfiguration.Property(o => o.CreatedDate).IsRequired();
            BaseEntityConfiguration.Property(o => o.UpdatedDate).IsRequired();
            BaseEntityConfiguration.Property(o => o.DeletedDate).IsRequired();

        }
    }
}
