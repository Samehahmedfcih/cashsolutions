﻿using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CashSolutions.BuildingBlocks.BaseEntities.Entities
{
    public class BaseEntity : IBaseEntity
    {
        public virtual string CreatedBy_Id { get; set; }
        public virtual string UpdatedBy_Id { get; set; }
        public virtual string DeletedBy_Id { get; set; }
        public string Tenant_Id { set; get; }
        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual DateTime DeletedDate { get; set; }
    }
}
