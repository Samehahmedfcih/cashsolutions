﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.VALIDATOR.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordViewModelValidator()
    {
            RuleFor(x => x.UserId).NotEmpty().NotNull();
            RuleFor(x => x.newPassword).Password();
    }
}
}
