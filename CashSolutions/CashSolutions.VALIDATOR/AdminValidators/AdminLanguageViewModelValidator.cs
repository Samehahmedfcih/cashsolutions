﻿using AMANAH.DMS.BLL.ViewModels.Admin;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class AdminLanguageViewModelValidator : AbstractValidator<AdminLanguageViewModel>
    {
        public AdminLanguageViewModelValidator()
        {
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.DashBoardLanguage).NotEmpty().NotNull();
            RuleFor(x => x.TrackingPanelLanguage).NotEmpty().NotNull();

        }
    }
}
