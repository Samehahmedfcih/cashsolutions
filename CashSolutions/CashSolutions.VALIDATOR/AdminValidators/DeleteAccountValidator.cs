﻿using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.VALIDATOR.Extensions;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class DeleteAccountValidator :AbstractValidator<AccountToDeleteViewModel>
    {
        public DeleteAccountValidator()
        {
            RuleFor(x => x.deactivationReason).NotNull().NotEmpty();
            RuleFor(x => x.Id).NotNull().NotEmpty();
            RuleFor(x => x.password).Password();
        }
    }
}
