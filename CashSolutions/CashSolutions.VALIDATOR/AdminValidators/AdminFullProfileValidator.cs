﻿using AMANAH.DMS.BLL.ViewModels.Admin;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class AdminFullProfileValidator : AbstractValidator<AdminFullProfile>
    {
        public AdminFullProfileValidator() {
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Id).NotEmpty().NotNull();
            RuleFor(x => x.CountryId).NotEmpty().NotNull();
        
        }
    }
}
