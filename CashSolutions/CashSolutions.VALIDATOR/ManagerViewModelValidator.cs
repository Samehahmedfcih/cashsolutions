﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class ManagerViewModelValidator : AbstractValidator<ManagerViewModel>
    {
        public ManagerViewModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().NotNull();
            RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
            RuleFor(x => x.Email).EmailAddress().NotEmpty().NotNull();
        }
    }

    public class CreateManagerViewModelValidator : AbstractValidator<CreateManagerViewModel>
    {
        public CreateManagerViewModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().NotNull();
            RuleFor(x => x.Password).NotEmpty().NotNull().MinimumLength(6).MaximumLength(100);
            RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
            RuleFor(x => x.Email).EmailAddress().NotEmpty().NotNull();
        }
    }
}
