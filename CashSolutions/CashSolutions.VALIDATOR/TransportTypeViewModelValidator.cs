﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{

	   public class TransportTypeViewModelValidator : AbstractValidator<TransportTypeViewModel>
	{
		public TransportTypeViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(50);
		}
	}
}
