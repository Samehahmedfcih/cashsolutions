﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class AgentTypeViewModelValidator : AbstractValidator<AgentTypeViewModel>
	{
		public AgentTypeViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
		}
	}
}
