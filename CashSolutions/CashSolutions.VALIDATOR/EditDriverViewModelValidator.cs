﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class EditDriverViewModelValidator : AbstractValidator<EditDriverViewModel>
    {
		public EditDriverViewModelValidator()
		{
			RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
			RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();   
			RuleFor(x => x.TeamId).NotEmpty();   
		}
	}
}
