﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;

namespace AMANAH.DMS.VALIDATOR
{
	public  class CountryViewModelValidator : AbstractValidator<CountryViewModel>
	{
		public CountryViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
		}
	}
}
