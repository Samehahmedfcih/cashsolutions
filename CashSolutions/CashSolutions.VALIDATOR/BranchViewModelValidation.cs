﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.VALIDATOR
{
    public class BranchViewModelValidation : AbstractValidator<BranchViewModel>
    {
        public BranchViewModelValidation()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            //RuleFor(x => x.Phone).NotEmpty().NotNull().MaximumLength(50);
            //RuleFor(x => x.RestaurantId).NotEmpty().NotNull();
            //RuleFor(x => x.GeoFenceId).NotEmpty().NotNull();
            //RuleFor(x => x.Address).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.Latitude).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.Longitude).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.ManagerId).NotEmpty().NotNull();


        }


    }
}
