﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR.Extensions
{
  
        public static class RuleBuilderExtensions
        {
            public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder, int minimumLength = 8)
            {
                var options = ruleBuilder
                    .NotEmpty().WithMessage("PasswordEmpty")
                    .MinimumLength(minimumLength).WithMessage(ErrorMessages.PasswordEmpty)
                    .Matches("[A-Z]").WithMessage(ErrorMessages.PasswordUppercaseLetter)
                    .Matches("[a-z]").WithMessage(ErrorMessages.PasswordLowercaseLetter)
                    .Matches("[0-9]").WithMessage(ErrorMessages.PasswordDigit)
                    .Matches("[!@#$%^&*]").WithMessage(ErrorMessages.PasswordSpecialCharacter);


                return options;
            }
        }
        public struct ErrorMessages
        {
            public static string PasswordEmpty = "Password Must be at Least 8 Characters";
            public static string PasswordUppercaseLetter = "Password Must Contain at least one Uppercase Letter";
            public static string PasswordLowercaseLetter = "Password Must Contain at least one lowercase Letter";
            public static string PasswordDigit = "Password Must Contain at least one digit";
            public static string PasswordSpecialCharacter = "Password Must Contain at least one Special Character !@#$%^&*";

        }
    
}
