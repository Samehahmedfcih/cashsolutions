﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class LkpViewModelValidator : AbstractValidator<LkpViewModel>
	{
		public LkpViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull();
		}
	}
}
