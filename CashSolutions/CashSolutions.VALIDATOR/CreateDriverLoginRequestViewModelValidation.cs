﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class CreateDriverLoginRequestViewModelValidation : AbstractValidator<CreateDriverLoginRequestViewModel>
    {
        public CreateDriverLoginRequestViewModelValidation()
        {
            RuleFor(x => x.DriverId).NotNull();
            RuleFor(x => x.Status).NotNull();
            RuleFor(x => x.Token).NotNull().NotEmpty();
            
        }
    }
}
