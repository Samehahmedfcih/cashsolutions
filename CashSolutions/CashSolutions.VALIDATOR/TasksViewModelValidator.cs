﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class TasksViewModelValidator : AbstractValidator<TasksViewModel>
	{
        public TasksViewModelValidator()
		{
			RuleFor(x => x.MainTaskId).NotEmpty().NotNull();
            RuleFor(x => x.TaskTypeId).NotEmpty().NotNull();
            RuleFor(x => x.Address).NotEmpty().NotNull();
            RuleFor(x => x.Customer).NotNull();

        }
    }
}
