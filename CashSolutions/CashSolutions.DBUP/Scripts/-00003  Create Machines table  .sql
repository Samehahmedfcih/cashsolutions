/****** Object:  Table [dbo].[Machines]    Script Date: 12/07/2020 11:13:02 م ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Machines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Model] [nvarchar](250) NOT NULL,
	[MachineId] [bigint] NULL,
	[BranchName] [nvarchar](150) NULL,
	[Version] [nvarchar](150) NULL,
	[IP] [nvarchar](150) NULL,
	[Tick] [bigint] NULL,
	[StatusId] [int] NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[Tenant_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
 CONSTRAINT [PK_Machines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

