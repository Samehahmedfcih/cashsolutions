﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using CashSolutions.Models.Entities;
using CashSolutions.Context;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using CashSolutions.Repoistry;

namespace CashSolutions.BLL.IManagers
{
    public class RolePrivilgeManager : Repositry<RolePrivilge>,IRolePrivilgeManager
    {
        public RolePrivilgeManager(ApplicationDbContext context , IHttpContextAccessor _httpContextAccessor)
            : base(context , _httpContextAccessor)
        {

        }
        public List<RolePrivilge> GetRolePrivilgeByRoleId(string roleId)
        {
            return GetAll().Where(role => role.FK_Role_Id == roleId).ToList();
        }
    }
}
