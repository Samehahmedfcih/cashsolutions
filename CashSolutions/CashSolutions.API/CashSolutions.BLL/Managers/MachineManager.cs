﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.ViewModels;
using CashSolutions.Context;
using CashSolutions.DATA.Entities;
using CashSolutions.Models.Entities;
using CashSolutions.Repoistry;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CashSolutions.BLL.Managers
{
    public class MachineManager : BaseManager<MachineViewModel, Machine>, IMachineManager
    {
        public MachineManager(ApplicationDbContext context, IMapper mapper, IRepositry<Machine> repository)
                               : base(context, repository, mapper)
        {
        }
   
        public async Task<MachineViewModel> Get(int id)
        {

          var machine=  await  (this.context as ApplicationDbContext).Machines.Where(x => x.Id == id).ProjectTo<MachineViewModel>().FirstOrDefaultAsync();

            return machine;

        }
    }
}
