﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.BLL.IManagers;
using CashSolutions.Context;
using CashSolutions.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utilites;
using CashSolutions.Repoistry;
using CashSolutions.BLL.Constants;
using CashSolutions.BLL.Models;
using CashSolutions.BLL.ViewModels;
using AutoMapper;
using CashSolutions.BLL.BLL.Settings;
using CashSolutions.BLL.BLL.IManagers;
using CashSolutions.BLL.ViewModels.Account;
using Microsoft.Extensions.Options;
using CashSolutions.Settings;
using IdentityModel.Client;
using CashSolutions.Models.Entities;
using System.Net;
using CashSolutions.BLL.Resources;
using Microsoft.Extensions.Configuration;
using CashSolutions.DATA.Entities;
using CashSolutions.BLL.DATA.Entities;

namespace CashSolutions.BLL.Managers
{
    public class ApplicationUserManager : IApplicationUserManager
    {
        //private readonly IPasswordHasher<ApplicationUser> _passwordHasher;//= new PasswordHasher<ApplicationUser>();
        public UserManager<ApplicationUser> _identityUserManager;
        RoleManager<ApplicationRole> _identityRoleManager;
        ApplicationDbContext _dbContext;
        public readonly IMapper _mapper;
        private readonly EmailSettings _emailSettings;
        private IEmailSenderservice _emailsender;
        private AppSettings _appSettings;
        protected readonly IApplicationUserManager _userManager;
        private readonly IUserDeviceManager _userDeviceManager;

        protected readonly IApplicationRoleManager _appllicationRoleManager;


        private static IdentityError UserNotFound = new IdentityError { Code = "UserNotFound", Description = "User doesn't exist" };
        private static IdentityError UserDeleted = new IdentityError { Code = "UserDeleted", Description = "User has been deleted" };
        private static IdentityError InvalidLogin = new IdentityError { Code = "InvalidLogin", Description = "Invalid login" };
        private static IdentityError InvalidTokenError = new IdentityError { Code = "InvalidToken", Description = "Invalid Token" };
        private static IdentityError GeneralError = new IdentityError { Code = "GeneralError", Description = "Un handled Exception" };
        IConfiguration _configuration;
        ITenantService tenantService { set; get; }

        public ApplicationUserManager(IApplicationRoleManager appllicationRoleManager, IConfiguration configuration, ApplicationDbContext dbContext, UserManager<ApplicationUser> identityUserManager, RoleManager<ApplicationRole> identityRoleManager, ITenantService _tenantService, IMapper mapper, IOptions<EmailSettings> emailSettings, IEmailSenderservice emailsender, IOptions<AppSettings> appSettings, IUserDeviceManager userDeviceManager)
        {
            _identityUserManager = identityUserManager;
            // _passwordHasher = passwordHasher;
            _identityRoleManager = identityRoleManager;
            _dbContext = dbContext;
            _mapper = mapper;
            tenantService = _tenantService;
            _emailSettings = emailSettings.Value;
            _emailsender = emailsender;
            _appSettings = appSettings.Value;
            _configuration = configuration;

            _userDeviceManager = userDeviceManager;
            _appllicationRoleManager = appllicationRoleManager;
        }


        /// <summary>
        /// Method to add user in the database using identity provider.
        /// </summary>
        /// <param name="user">user object to be added.</param>
        /// <returns>id of the added user in case of operation succeed. null in case of failure.</returns>

        public async Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users)
        {
            foreach (var usr in users)
            {
                await AddUserAsync(usr.Item1, usr.Item2);
            }

        }

        public async Task<UserManagerResult> AddTenantAsync(RegistrationViewModel model)
        {
            ApplicationUser applicationUser = new ApplicationUser()
            {
                FirstName = model.FullName,
                Email = model.Email,
                UserName = model.Email,
                RoleNames = new List<string> { StaticRoleNames.Tenant },
                PhoneNumber = model.PhoneNumber,
            };
            var res = await AddUserAsync(applicationUser, model.Password);
            if (res.Succeeded)
            {
                applicationUser.Tenant_Id = res.User.Id;
                await _identityUserManager.UpdateAsync(applicationUser);

                var emailBody = MailTemplates.RegistrationMail;
                var FrontEndURL = _configuration.GetValue<string>("FrontEndURL");


                var body = string.Format(emailBody, applicationUser.Email, FrontEndURL);

                try { 
                await _emailsender.SendEmailAsync("Welcome to Cash Solutions", body, res.User.Email);
                }
                catch (Exception e)
                {

                }
            }
            return res;
        }

        public async Task<UserManagerResult> AddUserAsync(string email, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser(new BaseEntity())
            {
                Email = email,
                UserName = email
            };
            return await AddUserAsync(applicationUser, password);
        }
        public async Task<UserManagerResult> AddUserAsync(ApplicationUser user, string password)
        {
            var result = new UserManagerResult();
            try
            {
                if (user.RoleNames != null && user.RoleNames.Count > 0)
                {
                    user.CreatedDate = DateTime.UtcNow;
                    user.SecurityStamp = Guid.NewGuid().ToString("D");
                    user.CreatedDate = DateTime.UtcNow;
                    var identityResult = await _identityUserManager.CreateAsync(user, password);
                    result = _mapper.Map<UserManagerResult>(identityResult);
                    if (identityResult.Succeeded)
                    {
                        result.User = _mapper.Map<UserViewModel>(user);
                        await AddUserToRolesAsync(user);
                    }
                    return result;
                }
                result.Errors.Add(new IdentityError { Code = "UserNoRole", Description = "User dosn't have any Role !" });

                return result;

            }
            catch (Exception ex)
            {
                result.Succeeded = false;
                //result. = ;
                return result;
            }

        }

        public async Task<LoginResult> LoginAsync(LoginViewModel model)
        {
            var result = new LoginResult();

            var user = await GetByEmailAsync(model.Email);
            if (user == null)
            {
                result.Errors.Add(UserNotFound);
                return result;
            }

            if (user.IsDeleted == true)
            {
                result.Errors.Add(UserDeleted);
                return result;
            }

            if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
            {
                var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                discoveryClient2.Policy.RequireHttps = false;
                discoveryClient2.Policy.ValidateIssuerName = false;
                var disco = await discoveryClient2.GetAsync();
                if (disco.TokenEndpoint != null)
                {
                    var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                    var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(model.Email, model.Password, "calling");
                    if (tokenResponse.AccessToken == null)
                    {
                        result.Errors.Add(InvalidLogin);
                        return result;
                    }
                    user.RoleNames = (await GetRolesAsync(user)).ToList();

                    var role = await  _appllicationRoleManager.GetRoleAsyncByName(user.RoleNames.FirstOrDefault(), user.Tenant_Id, RoleType.Manager);
                    if (role != null)
                    {
                        user.Permissions = (await _appllicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();
                    }
                    
                    result.User = _mapper.Map<ApplicationUser, UserViewModel>(user);

                    result.TokenResponse = tokenResponse;
                    return result;
                }
                else
                {
                    result.Errors.Add(InvalidLogin);
                    return result;
                }
            }

            result.Errors.Add(GeneralError);
            return result;
        }

        public async Task<ApplicationUser> GetByUserNameOrEmailAsync(string usernameOrEmail)
        {
            var user = await _identityUserManager.FindByNameAsync(usernameOrEmail);
            if (user == null)
            {
                user = await _identityUserManager.FindByEmailAsync(usernameOrEmail);
                if (user == null)
                {
                    return null;
                }
            }

            return user.IsDeleted ? null : user;
        }

        public async Task<AgentLoginResult> DriverLoginAsync(AgentLoginViewModel model)
        {
            var result = new AgentLoginResult();
            try
            {

                var user = await GetByUserNameOrEmailAsync(model.Email);

                if (user == null)
                {

                    result.Succeeded = false;
                    result.Errors.Add(new IdentityError() { Code = "UserNotFound", Description = "This user dosen't exist" });
                    return result;
                }

                if (user.IsDeleted == true)
                {

                    result.Succeeded = false;
                    result.Errors.Add(new IdentityError() { Code = "UserDeleted", Description = "This user has been deleted" });
                    return result;
                }

                //var driver = this._dbContext.Driver.FirstOrDefault(x => x.UserId == user.Id);
                //if(driver == null)
                //{
                //    result.Succeeded = false;
                //    result.Errors.Add(new IdentityError() { Code = "NotDriver", Description = "This user is not registerd as Driver" });
                //    return result;
                //}

                //if(driver.AgentStatusId == (int)Enums.AgentStatuses.Block)
                //{
                //    result.Succeeded = false;
                //    result.Errors.Add(new IdentityError() { Code = "NotDriver", Description = "This user is blocked" });
                //    return result;
                //}


                if (user == null)
                {
                    result.Errors.Add(UserNotFound);
                    return result;
                }
           ;

                if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
                {
                    var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                    discoveryClient2.Policy.RequireHttps = false;
                    discoveryClient2.Policy.ValidateIssuerName = false;
                    var disco = await discoveryClient2.GetAsync();
                    if (disco.TokenEndpoint != null)
                    {
                        var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                        var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(user.UserName, model.Password, "calling");
                        if (tokenResponse.AccessToken == null)
                        {
                            result.Errors.Add(UserNotFound);
                            return result;
                        }
                        user.RoleNames = (await GetRolesAsync(user)).ToList();

                        var role = await _appllicationRoleManager.GetRoleAsyncByName(user.RoleNames.FirstOrDefault(), user.Tenant_Id, RoleType.Agent);
                        if (role != null)
                        {
                            user.Permissions = (await _appllicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();
                        }


                        _userDeviceManager.AddIfNotExist(new UserDevice { FCMDeviceId = model.FCMDeviceId, UserId = user.Id, Version = model.Version, DeviceType = model.DeviceType });

                        result.User = _mapper.Map<ApplicationUser, UserViewModel>(user);
                        result.TokenResponse = tokenResponse;
                        return result;
                    }
                }

                result.Errors.Add(GeneralError);
                return result;
            }
            catch (Exception e)
            {

                result.Succeeded = false;
                result.Errors.Add(new IdentityError() { Code = "UnhandeledException", Description = "Un handled exception" });
                return result;

            }


        }



        public async Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
        {
            bool result = false;
            IdentityResult identityResult = await _identityUserManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;

        }

        public async Task<bool> UpdateUserAsync(ApplicationUser user)
        {
            bool result = false;
            var olduser = await _identityUserManager.FindByIdAsync(user.Id);
            var oldRoles = (await GetRolesAsync(olduser.UserName)).ToList();
            var identityRMVRolResult = await _identityUserManager.RemoveFromRolesAsync(olduser, oldRoles);
            if (identityRMVRolResult.Succeeded)
            {
                CopyProps(olduser, user);
                olduser.UpdatedDate = DateTime.UtcNow;
                IdentityResult identityResult = await _identityUserManager.UpdateAsync(olduser);
                if (identityResult.Succeeded)
                {
                    result = true;

                    if (olduser.RoleNames != null)
                    {
                        result = true;
                        var identityAddRolResult = await _identityUserManager.AddToRolesAsync(olduser, user.RoleNames);
                        if (identityAddRolResult.Succeeded)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            else
            {
                result = false;
            }
            return result;
        }

        public Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {



            return _identityUserManager.UpdateAsync(user);
        }

        public async Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable)
        {
            bool result = false;
            var user = await _identityUserManager.FindByIdAsync(userId);
            user.IsAvailable = isAvailable;
            var identityResult = await _identityUserManager.UpdateAsync(user);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;
        }

        public string HashPassword(ApplicationUser applicationUser, string newPassword)
        {
            return _identityUserManager.PasswordHasher.HashPassword(applicationUser, newPassword);
        }

        public async Task<ApplicationUser> GetByEmailAsync(string email)
        {
            ApplicationUser user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                if (user.IsDeleted)
                {
                    return null;
                }
            }

            return user;
        }

        public async Task<ApplicationUser> GetByUserIdAsync(string userId)
        {
            ApplicationUser user = await _identityUserManager.FindByIdAsync(userId);

            if (user != null)
            {
                if (user.IsDeleted)
                {
                    return null;
                }
            }
            return user;
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            var result = false;
            user = await GetAsync(user);
            var callBack = await _identityUserManager.DeleteAsync(user);
            if (callBack.Succeeded)
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> SoftDeleteAsync(ApplicationUser user)
        {
            user = await GetAsync(user);
            user.Email = user.Email + "_Deleted";

            user.UserName = user.UserName + "_Deleted";

            user.IsDeleted = true;
            user.DeletedDate = DateTime.UtcNow;


            var result = await _identityUserManager.UpdateAsync(user);
            if (result.Succeeded)
                return true;

            return false;
        }

        public async Task<ApplicationUser> GetAsync(ApplicationUser user)
        {
            if (user.UserName != null)
                user = await _identityUserManager.FindByNameAsync(user.UserName);
            else if (user == null && user.Email != null)
                user = await _identityUserManager.FindByEmailAsync(user.Email);
            else if (user == null && user.Id != null)
                user = await _identityUserManager.FindByIdAsync(user.Id);
            if (user != null)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }


            if (user != null)
            {
                if (user.IsDeleted)
                {
                    return null;
                }
            }
            return user;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var result = _identityUserManager.Users.Where(x => x.IsDeleted != true && x.Tenant_Id == tenantService.GetCurrentTenant_Id()).OrderByDescending(x => x.CreatedDate).ToList();
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                    // result.Add(user);
                }
            }
            return result;
        }

        public List<ApplicationUser> GetByUserIds(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => userIds.Contains(usr.Id) && usr.IsDeleted != true && usr.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
        }
        public List<ApplicationUser> GetAllExcept(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => !userIds.Contains(usr.Id) && usr.IsDeleted != true && usr.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
        }

        public async Task<List<ApplicationUser>> GetAdmins(List<string> userIdsExclude = null)
        {
            return await GetByRole(StaticRoleNames.Admin);
        }

        public async Task<List<ApplicationUser>> GetByRole(string roleName, List<string> userIdsExclude = null)
        {
            var result = (await _identityUserManager.GetUsersInRoleAsync(roleName)).Where(x => x.IsDeleted != true && x.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
            if (userIdsExclude != null)
            {
                result = result.Where(usr => !userIdsExclude.Contains(usr.Id) && usr.IsDeleted != true && usr.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
            }
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                }
            }
            return result;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        public async Task<IList<string>> GetRolesAsync(string userName)
        {

            var user = await GetByUserNameOrEmailAsync(userName);
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<bool> AddUserToRoleAsync(string userName, string roleName)
        {
            bool result = false;
            var applicationUser = await GetByEmailAsync(userName);
            if (applicationUser != null)
            {
                if (roleName != null)
                {
                    var role = await _identityRoleManager.FindByNameAsync(roleName);
                    IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, role.Name);
                    if (roleResult.Succeeded)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }

            }
            return result;
        }

        public async Task AddUsersToRolesAsync(List<ApplicationUser> users)
        {
            foreach (var user in users)
            {
                await AddUserToRolesAsync(user);
            }
        }
        public async Task AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                foreach (var role in user.RoleNames)
                {
                    if (await _identityRoleManager.FindByNameAsync(role) == null)
                    {
                        await _identityRoleManager.CreateAsync(new ApplicationRole() { Name = role });
                    }
                }
                await _identityUserManager.AddToRolesAsync(user, user.RoleNames);
            }
        }
        public async Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole)
        {
            bool result = false;
            if (applicationUser != null)
            {
                IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, applicationRole.Name);
                if (roleResult.Succeeded)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void CopyProps(ApplicationUser oldUser, ApplicationUser user)
        {
            oldUser.PhoneNumber = user.PhoneNumber;
            oldUser.RoleNames = user.RoleNames;
            oldUser.UserName = user.UserName;
            oldUser.Email = user.Email;
            oldUser.FirstName = user.FirstName;
            oldUser.MiddleName = user.MiddleName;
            oldUser.LastName = user.LastName;
            oldUser.RoleNames = user.RoleNames;

        }
        public async Task<bool> IsAllowToDeleteAdmin()
        {
            try
            {
                var admincount = (await _identityUserManager.GetUsersInRoleAsync("Admin")).Where(x => x.IsDeleted != true && x.Tenant_Id == tenantService.GetCurrentTenant_Id()).Count();

                if (admincount > 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception s)
            {
                return false;
            }
        }
        public async Task<bool> IsPhoneExist(string Phone)
        {
            try
            {

                return await Task.Run(() =>
                {
                    return _dbContext.Users.Any(x => x.PhoneNumber == Phone && x.Tenant_Id == tenantService.GetCurrentTenant_Id());
                });

            }
            catch (Exception s)
            {
                return false;
            }
        }

        public async Task<bool> IsUserNameExistAsync(string userName, string id)
        {

            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                if (user.Id != id)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {

                return false;
            }
        }

        public async Task<UserManagerResult> ForgotPassword(string email)
        {
            var result = new UserManagerResult();

            try
            {
                var user = await GetByEmailAsync(email);
                if (user == null)
                {
                    result.Errors.Add(UserNotFound);
                    return result;
                }

                var token = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
                token = WebUtility.UrlEncode(token);
                var resetUrl = string.Format(_emailSettings.ResetCallBackUrl, token, user.UserName);
                //resetUrl = WebUtility.UrlEncode(resetUrl);
                var callbackUrl = "<a href='" + resetUrl + "'>Reset Password</a>";
                var emailBody = MailTemplates.ForgetPasswordEmail;


                var body = string.Format(emailBody, email, resetUrl);


                await _emailsender.SendEmailAsync("Cash Password Reset", body, user.Email);
                return result;
            }
            catch (Exception e)
            {
                result.Errors.Add(new IdentityError { Code = "SendResetEmailError", Description = "Can't send Reset Email" });
                return result;
            }
        }
        public async Task<UserManagerResult> ResetPasswordAsync(string email, string token, string newPassword)
        {
            var result = new UserManagerResult();
            var user = await GetByEmailAsync(email);
            if (user == null)
            {
                result.Errors.Add(UserNotFound);
                return result;
            }
            var res = await _identityUserManager.ResetPasswordAsync(user, token, newPassword);
            result = _mapper.Map<UserManagerResult>(res);
            return result;
        }
        public async Task<string> GeneratePasswordResetTokenAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            return await _identityUserManager.GeneratePasswordResetTokenAsync(user);
        }
        public async Task<UserManagerResult> CheckResetToken(string email, string token)
        {
            var result = new UserManagerResult();
            var user = await GetByEmailAsync(email);
            if (user == null)
            {
                result.Errors.Add(InvalidTokenError);
                return result;
            }
            var valid = await _identityUserManager.VerifyUserTokenAsync(user, _identityUserManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", token);
            if (!valid)
            {
                result.Errors.Add(InvalidTokenError);
            }
            return result;

        }
        public async Task<bool> ChangePasswordAsync(ApplicationUser user, string newPassword)
        {
            await _identityUserManager.RemovePasswordAsync(user);
            var res = await _identityUserManager.AddPasswordAsync(user, newPassword);
            return res.Succeeded;
        }

    }
}
