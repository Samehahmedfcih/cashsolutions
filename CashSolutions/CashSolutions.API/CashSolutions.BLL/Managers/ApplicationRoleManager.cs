﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.BLL.IManagers;
using CashSolutions.Entities;
using CashSolutions.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CashSolutions.Repoistry;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using CashSolutions.Context;
using CashSolutions.DATA.Entities;
using CashSolutions.BLL.DATA.Entities;

namespace CashSolutions.BLL.Managers
{
    public class ApplicationRoleManager : IApplicationRoleManager
    {
        RoleManager<ApplicationRole> identityRoleManager;
        IRolePrivilgeManager rolePrivilgeManager;
        IPrivilgeManager privilgeManager;
        private readonly ApplicationDbContext _dbContext;

        ITenantService tenantService { set; get; }


        public ApplicationRoleManager(
            RoleManager<ApplicationRole> _identityRoleManager,
            IRolePrivilgeManager _rolePrivilgeManager,
            IPrivilgeManager _privilgeManager,
            ITenantService _tenantService,
            ApplicationDbContext dbContext
            )
        {
            identityRoleManager = _identityRoleManager;
            rolePrivilgeManager = _rolePrivilgeManager;
            privilgeManager = _privilgeManager;
            tenantService = _tenantService;
            _dbContext = dbContext;
        }
        public List<Privilge> GetPrivilgesByRoleId(string roleId)
        {
            List<Privilge> result = new List<Privilge>();
            var rolePrivilges = rolePrivilgeManager.GetAll().Where(r => r.FK_Role_Id == roleId && r.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var privilge = privilgeManager.Get(rolePrivilge.FK_Privilge_Id);
                if (privilge != null)
                {
                    result.Add(privilge);
                }
            }
            return result;
        }
        public async Task<List<ApplicationRole>> GetRolesByPrivilgeId(int privilgeId)
        {
            List<ApplicationRole> result = new List<ApplicationRole>();
            var rolePrivilges = rolePrivilgeManager.GetAll().Where(r => r.FK_Privilge_Id == privilgeId && r.Tenant_Id == tenantService.GetCurrentTenant_Id()).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var role = await GetRoleAsyncById(rolePrivilge.FK_Role_Id);
                if (role != null)
                {
                    result.Add(role);
                }
            }
            return result;
        }
        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName, RoleType type = (RoleType)1)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Name == roleName && t.Tenant_Id == tenantService.GetCurrentTenant_Id() && t.Type == (int)type);
        }

        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName, string tenantId, RoleType type = (RoleType)1 )
        {
            return await _dbContext.Roles.FirstOrDefaultAsync(t => t.Name == roleName && t.Tenant_Id == tenantId && t.Type == (int)type);
        }

        public async Task<ApplicationRole> GetRoleAsyncById(string Id)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Id == Id && t.Tenant_Id == tenantService.GetCurrentTenant_Id());
        }
        public async Task<ApplicationRole> GetRoleAsync(ApplicationRole role)
        {
            ApplicationRole result = null;
            if (!string.IsNullOrEmpty(role.Name))
                result = await GetRoleAsyncByName(role.Name);
            if (result == null)
            {
                if (!string.IsNullOrEmpty(role.Id))
                    result = await GetRoleAsyncById(role.Id);
            }
            return result;
        }
        public List<ApplicationRole> GetAllRoles(RoleType type = (RoleType)1)
        {

            if (type == RoleType.General)
            {
                return identityRoleManager.Roles.Where(r => r.Tenant_Id == tenantService.GetCurrentTenant_Id() ).ToList();
            }
            else { 

            return identityRoleManager.Roles.Where(r => r.Tenant_Id == tenantService.GetCurrentTenant_Id() && r.Type == (int)type).ToList();
}
        }

        public async Task<string> AddRoleAsync(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole.Name;
            }
            else
            {
                throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
            }
        }
        public async Task AddRolesAsync(List<ApplicationRole> applicationRoles)
        {
            foreach (var role in applicationRoles)
            {
                await AddRoleAsync(role);
            }
        }
        public async Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole;
            }
            else
            {
                throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
            }
        }

        public async Task<bool> UpdateRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var oldRole = await GetRoleAsync(applicationRole);
            if (applicationRole != null)
            {
                oldRole.Name = applicationRole.Name;
                IdentityResult callBack = await identityRoleManager.UpdateAsync(oldRole);
                if (callBack.Succeeded)
                {
                    result = true;
                }
                else
                {
                    if (callBack.Errors.Count() > 0)
                        throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));
                    return false;
                }
            }
            return result;
        }

        public async Task<bool> DeleteRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var role = await GetRoleAsync(applicationRole);
            IdentityResult callBack = await identityRoleManager.DeleteAsync(role);
            if (callBack.Succeeded)
            {
                result = true;
            }
            else
            {
                if (callBack.Errors.Count() > 0)
                    throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));

                return false;
            }
            return result;
        }

        public async Task<string> AddRoleAsync(string roleName, RoleType type = (RoleType)1)
        {
            ApplicationRole applicationRole = new ApplicationRole(new BaseEntity(), roleName);
            applicationRole.Type = (int)type;
            applicationRole.Tenant_Id = tenantService.GetCurrentTenant_Id();
            return await AddRoleAsync(applicationRole);
        }

        public async Task<bool> IsRoleExistAsync(string roleName)
        {
            return await _dbContext.Roles.AnyAsync(t => t.Name == roleName);
        }

        public async Task<bool> IsRoleExistAsync(string roleName, RoleType type = (RoleType)1)
        {
            return await _dbContext.Roles.AnyAsync(t => t.Name == roleName && t.Type == (int)type);
        }

        public async Task<List<Privilge>> GetPrivilgesByRoleName(string roleName, RoleType type = (RoleType)1)
        {
            List<Privilge> result = null;
            var role = await GetRoleAsyncByName(roleName, type);
            if (role != null)
            {
                result = GetPrivilgesByRoleId(role.Id);
            }
            return result;
        }

        public async Task<bool> AddClaimAsync(ApplicationRole role, Claim claim)
        {
            var result = await identityRoleManager.AddClaimAsync(role, claim);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                if (result.Errors.Count() > 0)
                    throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));

                return false;
            }
        }
        public async Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName)
        {
            var roleClaim = _dbContext.RoleClaims.SingleOrDefault(t => t.ClaimValue == claimName && t.RoleId == role.Id);
            _dbContext.Remove(roleClaim);
            _dbContext.SaveChanges();
            return true;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationRole role)
        {
            var claims = await _dbContext.RoleClaims.Where(t => t.RoleId == role.Id).Select(t => t.ToClaim()).ToListAsync();
            return claims;
        }

        public List<string> GetAllRolesByUserId(string userId)
        {
            var roleIds = _dbContext.UserRoles.Where(r => r.UserId == userId).Select(r => r.RoleId).ToList();
            return _dbContext.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToList();
        }
    }
}
