﻿
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using CashSolutions.Repoistry;
using CashSolutions.Models.Entities;
using CashSolutions.Context;
using CashSolutions.BLL.IManagers;
using Microsoft.AspNetCore.Http;

namespace CashSolutions.BLL.Managers
{
    public class PrivilgeManager : Repositry<Privilge>, IPrivilgeManager
    {
        IRolePrivilgeManager privilgeManager;
        public PrivilgeManager(ApplicationDbContext context, IRolePrivilgeManager _privilgeManager, IHttpContextAccessor _httpContextAccessor)
            : base(context , _httpContextAccessor)
        {


            privilgeManager = _privilgeManager;
        }

    }
}
