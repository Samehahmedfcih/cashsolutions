﻿using CashSolutions.BASE;
using CashSolutions.BLL.Authorization;
using CashSolutions.BLL.DATA.Entities;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.ViewModel;
using CashSolutions.BLL.ViewModels;
using CashSolutions.DATA.Entities;
using CashSolutions.Repoistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CashSolutions.BLL.Managers
{
    public class AgentAccessControlManager : IAgentAccessControlManager
    {
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ITenantService _tenantService;

        public AgentAccessControlManager(IApplicationRoleManager applicationRoleManager, ITenantService tenantService)
        {
            _applicationRoleManager = applicationRoleManager;
            _tenantService = tenantService;
        }
        public async Task<bool> Create(AgentAccessControlViewModel input)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(input.RoleName))
            {
                throw new Exception("Role Exist before please check name");
            }
            else
            {
                var _role = new Entities.ApplicationRole();
                _role.Name = input.RoleName;
                _role.CreatedDate = DateTime.UtcNow;

                _role.Tenant_Id = _tenantService.GetCurrentTenant_Id();
                _role.Type = (int)RoleType.Agent;
                await _applicationRoleManager.AddRoleAsync(_role);

                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Agent);
                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }

        }

        public async Task<bool> Delete(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Agent))
            {
                throw new NotFoundException("Role Not Exist");
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Agent);
                if (role != null)
                    return await _applicationRoleManager.DeleteRoleAsync(role);
                return false;
            }
        }

        public async Task<AgentAccessControlViewModel> Get(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Agent))
            {
                throw new NotFoundException("Role Not Exist");
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Agent);
                return new AgentAccessControlViewModel
                {
                    RoleName = role.Name,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList()
                };
            }
        }

        public async Task<List<AgentAccessControlViewModel>> GetAll()
        {
            List<AgentAccessControlViewModel> lst = new List<AgentAccessControlViewModel>();
            var roles = _applicationRoleManager.GetAllRoles(RoleType.Agent);

            foreach (var r in roles)
            {


                lst.Add(new AgentAccessControlViewModel
                {
                    RoleName = r.Name,
                    CreationDate = r.CreatedDate,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList()
                });


            }


            //roles.ForEach(async r =>
            //{
            //    lst.Add(new AgentAccessControlViewModel
            //    {
            //        CreationDate = r.CreatedDate,
            //        RoleName = r.Name, Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList() });
            //});
            return lst.OrderByDescending(x => x.CreationDate).ToList();
        }

        public async Task<List<FeaturePermissionViewModel>> GetAllPermissions()
        {
            List<FeaturePermissionViewModel> lst = new List<FeaturePermissionViewModel>();


            ////FeaturePermissionViewModel TasksPermission = new FeaturePermissionViewModel();

            ////TasksPermission.Name = "Task  Permissions";
            ////TasksPermission.Permissions = new List<PermissionViewModel>();
            ////TasksPermission.Permissions.Add(new PermissionViewModel() { Name = "Create Task ", Value = AgentPermissions.Task.CreateTask });
            ////TasksPermission.Permissions.Add(new PermissionViewModel() { Name = "Update Task ", Value = AgentPermissions.Task.UpdateTask });
            ////TasksPermission.Permissions.Add(new PermissionViewModel() { Name = "Change Task Status ", Value = AgentPermissions.Task.ChangeTaskStatus });
            ////TasksPermission.Permissions.Add(new PermissionViewModel() { Name = "Create Unassigned Task ", Value = AgentPermissions.Task.CreateUnassignedTask });



            FeaturePermissionViewModel ProfilePermission = new FeaturePermissionViewModel();

            ProfilePermission.Name = "Profile Permissions";
            ProfilePermission.Permissions = new List<PermissionViewModel>();
            ProfilePermission.Permissions.Add(new PermissionViewModel() { Name = "Update Profile ", Value = AgentPermissions.Profile.UpdateProfile });
            ProfilePermission.Permissions.Add(new PermissionViewModel() { Name = "Update Profile Picture ", Value = AgentPermissions.Profile.UpdateProfilePicture });

            //lst.Add(TasksPermission);
            lst.Add(ProfilePermission);



            return lst;
        }

        public async Task<bool> Update(AgentAccessControlViewModel input)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(input.RoleName, RoleType.Agent))
            {
                throw new NotFoundException("Role Not Exist");
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Agent);
                var existsClaims = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();

                existsClaims.ForEach(c =>
                {
                    _applicationRoleManager.RemoveClaimAsync(role, c);
                });

                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }
        }
    }
}
