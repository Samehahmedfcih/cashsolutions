﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CashSolutions.BLL.BLL.IManagers;
using CashSolutions.BLL.BLL.Settings;
using CashSolutions.BLL.IManagers;
using CashSolutions.Entities;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using MimeKit;

namespace CashSolutions.BLL.Managers
{
    public class EmailSenderService : IEmailSenderservice
    {
        private readonly EmailSettings _emailSettings;
        private readonly IHostingEnvironment _env;
        public EmailSenderService(IOptions<EmailSettings> emailSettings,IHostingEnvironment env)
        {
            _emailSettings = emailSettings.Value;
            _env = env;
        }

        public async Task SendEmailAsync(string subject, string body, string toEmail)
        {
            await SendEmailAsync(subject, body, new List<string> { toEmail });
        }
        public async Task SendEmailAsync(string subject, string body, IList<string> toEmails)
        {            
            try
            {
                var mimeMessage = new MimeMessage();

                mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName,_emailSettings.Sender));
                if (toEmails != null)
                {
                    foreach (var email in toEmails)
                    {
                        mimeMessage.To.Add(new MailboxAddress("", email));
                    }
                }
                //mimeMessage.To.Add(new MailboxAddress("",));

                mimeMessage.Subject = subject;

                mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = body
                };

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    //if (_env.IsDevelopment())
                    //{
                    //    // The third parameter is useSSL (true if the client should make an SSL-wrapped
                    //    // connection to the server; otherwise, false).
                    //    await client.ConnectAsync("smtp.some_server.com", 587, true);
                    //}
                    //else
                    //{
                    await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, false);
                    //}

                    // Note: only needed if the SMTP server requires authentication
                    await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);

                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }

            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }

        }
        }
    }
