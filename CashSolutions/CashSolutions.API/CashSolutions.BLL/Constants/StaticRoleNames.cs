﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.Constants
{
    public static class StaticRoleNames
    {
        public const string Tenant = "Tenant";
        public const string Admin = "Admin";
    }
}
