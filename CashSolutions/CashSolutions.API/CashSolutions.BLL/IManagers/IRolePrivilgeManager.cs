﻿using CashSolutions.Models.Entities;
using CashSolutions.Repoistry;
using System.Collections.Generic;

namespace CashSolutions.BLL.IManagers
{
    public interface IRolePrivilgeManager : IRepositry<RolePrivilge>
    {
        List<RolePrivilge> GetRolePrivilgeByRoleId(string roleId);
    }
}
