﻿using CashSolutions.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CashSolutions.BLL.BLL.IManagers
{
   public interface IEmailSenderservice
    {
        Task SendEmailAsync(string subject, string body, string toEmail);
        Task SendEmailAsync(string subject, string body, IList<string> toEmails);
    }
}
