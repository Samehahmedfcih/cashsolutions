﻿using CashSolutions.BLL.ViewModels;
using CashSolutions.DATA.Entities;
using CashSolutions.Models.Entities;
using CashSolutions.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CashSolutions.BLL.IManagers
{
    public interface IMachineManager : IBaseManager<MachineViewModel, Machine>
    {

        Task<MachineViewModel> Get(int id);
    }
}
