﻿using CashSolutions.BLL.DATA.Entities;
using CashSolutions.DATA.Entities;
using CashSolutions.Entities;
using CashSolutions.Models.Entities;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CashSolutions.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> GetRoleAsyncByName(string roleName, RoleType type = (RoleType)1);

        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);

        Task<string> AddRoleAsync(ApplicationRole applicationRole);

        Task AddRolesAsync(List<ApplicationRole> applicationRoles);

        Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole);

        Task<string> AddRoleAsync(string roleName, RoleType type = (RoleType)1);

        Task<bool> IsRoleExistAsync(string roleName);

        Task<bool> IsRoleExistAsync(string roleName, RoleType type = (RoleType)1);

        Task<List<Privilge>> GetPrivilgesByRoleName(string roleName, RoleType type = (RoleType)1);

        List<ApplicationRole> GetAllRoles( RoleType type = (RoleType)1);

        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);

        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);

        Task<bool> AddClaimAsync(ApplicationRole role, Claim claim);

        Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName);

        //Task<List<string>> GetClaimsAsync(ApplicationRole role);

        Task<IList<Claim>> GetClaimsAsync(ApplicationRole role);

        List<string> GetAllRolesByUserId(string userId);

         Task<ApplicationRole> GetRoleAsyncByName(string roleName, string tenantId, RoleType type = (RoleType)1);
    }
}