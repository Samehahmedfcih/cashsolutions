﻿// Decompiled with JetBrains decompiler
// Type: CashSolutions.BLL.IManagers.IApplicationUserManager
// Assembly: CashSolutions.BLL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 131D4CEF-54A8-4912-B5F9-62DFE25D1ABE
// Assembly location: D:\EnmaaBKp\Enmaa\Identity\CashSolutions.BLL.dll

using CashSolutions.BLL.Models;
using CashSolutions.BLL.ViewModels;
using CashSolutions.BLL.ViewModels.Account;
using CashSolutions.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CashSolutions.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);

        Task<UserManagerResult> AddUserAsync(ApplicationUser user, string password);

        Task<UserManagerResult> AddTenantAsync(RegistrationViewModel model);

        Task<UserManagerResult> AddUserAsync(string email, string password);

        Task<LoginResult> LoginAsync(LoginViewModel model);


        Task<AgentLoginResult> DriverLoginAsync(AgentLoginViewModel model);

        Task<bool> UpdateUserAsync(ApplicationUser user);

        Task<IdentityResult> UpdateAsync(ApplicationUser user);

        Task<ApplicationUser> GetByEmailAsync(string email);

        Task<ApplicationUser> GetByUserIdAsync(string userId);

        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);

        Task<IList<string>> GetRolesAsync(string userName);

        Task<IList<string>> GetRolesAsync(ApplicationUser user);

        Task<bool> AddUserToRoleAsync(string userName, string roleName);

        Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole);

        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsUserNameExistAsync(string userName , string id);


        Task<bool> IsEmailExistAsync(string email);

        Task<bool> DeleteAsync(ApplicationUser user);

        Task<bool> SoftDeleteAsync(ApplicationUser user);

        Task<ApplicationUser> GetAsync(ApplicationUser user);

        Task<List<ApplicationUser>> GetAll();

        List<ApplicationUser> GetByUserIds(List<string> userIds);

        List<ApplicationUser> GetAllExcept(List<string> userIds);

        Task<List<ApplicationUser>> GetAdmins(List<string> userIdsExclude = null);

        Task<List<ApplicationUser>> GetByRole(string roleName, List<string> userIdsExclude = null);

        Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable);

        string HashPassword(ApplicationUser applicationUser, string newPassword);

        Task<bool> IsAllowToDeleteAdmin();

        Task<bool> IsPhoneExist(string Phone);

        Task<UserManagerResult> ForgotPassword(string email);
        Task<UserManagerResult> ResetPasswordAsync(string email, string token, string newPassword);
        Task<UserManagerResult> CheckResetToken(string email, string token);
        Task<ApplicationUser> GetByUserNameOrEmailAsync(string usernameOrEmail);
        Task<string> GeneratePasswordResetTokenAsync(string email);

        Task<bool> ChangePasswordAsync(ApplicationUser user, string newPassword); 
        Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);


    }
}
