﻿using CashSolutions.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        void AddIfNotExist(UserDevice userDevice);
        List<UserDevice> GetByUserId(string userId);
        bool DeleteDevice(string token);
    }
}
