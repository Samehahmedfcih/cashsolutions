﻿using CashSolutions.BLL.ViewModel;
using CashSolutions.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CashSolutions.BLL.IManagers
{
    public interface IAgentAccessControlManager
    {
        Task<bool> Create(AgentAccessControlViewModel input);
        Task<bool> Update(AgentAccessControlViewModel input);
        Task<bool> Delete(string roleName);
        Task<AgentAccessControlViewModel> Get(string roleName);
        Task<List<AgentAccessControlViewModel>> GetAll();
      Task<List<FeaturePermissionViewModel>> GetAllPermissions();

    }
}
