﻿using CashSolutions;
using CashSolutions.BLL.ViewModel;
using CashSolutions.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.ViewModel
{
    public class PrivilgeViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationRoleViewModel> Roles { get; set; }
    }
}
