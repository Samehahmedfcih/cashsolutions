﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
   public class AgentAccessControlViewModel
    {
        public DateTime? CreationDate { set; get; }

        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
    }
}
