﻿using CashSolutions.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
    public class MachineStatusViewModel : BaseViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
