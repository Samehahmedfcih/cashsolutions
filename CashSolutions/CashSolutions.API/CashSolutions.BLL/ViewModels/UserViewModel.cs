﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }

        public List<string> RoleNames { get; set; }
        public List<string> Permissions { get; set; }
        public string FullName { get; set; }
    }
}
