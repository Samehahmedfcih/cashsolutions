﻿using CashSolutions.BASE;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
    public class BackgroundJobViewModel<T>
    {
        public readonly MainTaskViewModelClaimsPrincipal CustomerHttpContextUser;
        public BackgroundJobViewModel(MainTaskViewModelClaimsPrincipal customerHttpContextUser)
        {
            CustomerHttpContextUser = customerHttpContextUser;
        }
        public BackgroundJobViewModel(IHttpContextAccessor httpContextAccessor)
        {
            CustomerHttpContextUser = new MainTaskViewModelClaimsPrincipal
            {
                Tenant_Id = httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value,
                UserName = httpContextAccessor.HttpContext.User.Identity.Name
            };
        }

        public T ViewModel { get; set; }
    }
}
