﻿using CashSolutions.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
    public class MachineViewModel : BaseViewModel 
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public long MachineId { get; set; }
        public string BranchName { get; set; }
        public string Version { get; set; }
        public string IP { get; set; }
        public long Tick { get; set; }
        public long MachineStatusId { get; set; }
        public MachineStatusViewModel MachineStatus { set; get; }

    }
}
