﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Admin
{
    public class AccountToDeleteViewModel
    {
        public string Id { get; set; }
        public string  deactivationReason { get; set; }
        public string password { get; set; }
    }
}
