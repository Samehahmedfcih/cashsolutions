﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Admin
{
    public class AdminLanguageViewModel
    {
        public string Id { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; } 
    }
}
