﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Account
{
    public class LoginResult : UserManagerResult
    {
        public TokenResponse TokenResponse { get; set; }
    }
}
