﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Account
{
    public class CheckResetTokenRequest
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
