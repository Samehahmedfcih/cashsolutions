﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Account
{
    public class AgentLoginResult : LoginResult
    {

       public string ProfilePictureURL { set; get; }
      
        public int  DriverID { set; get; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
        public int? AgentStatusId { set; get; }
        public int NewTasksCount { get; set; }

        public string LocationAccuracyName { set; get; }
        public int? LocationAccuracyDuration { set; get; }
    }
}
