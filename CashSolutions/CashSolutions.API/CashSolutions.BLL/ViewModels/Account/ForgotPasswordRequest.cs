﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels.Account
{
    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }
}
