﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string UserId { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }
}
