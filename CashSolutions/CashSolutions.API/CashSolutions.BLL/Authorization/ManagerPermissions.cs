﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CashSolutions.BLL.Authorization
{
    public static class ManagerPermissions
    {
        public static readonly IEnumerable<string> OrderPermissions;
        public static readonly IEnumerable<string> AgentPermissions;
        public static readonly IEnumerable<string> CustomerPermissions;
        public static readonly IEnumerable<string> TeamPermissions;
        public static readonly IEnumerable<string> SettingsPermissions;
        static ManagerPermissions()
        {
            Type taskType = typeof(Task);
            var taskFlags = BindingFlags.Static | BindingFlags.Public;
            OrderPermissions = taskType.GetFields(taskFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type Machine = typeof(Agent);
            var agentFlags = BindingFlags.Static | BindingFlags.Public;
            AgentPermissions = Machine.GetFields(agentFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type customerType = typeof(Customer);
            var customerFlags = BindingFlags.Static | BindingFlags.Public;
            CustomerPermissions = customerType.GetFields(customerFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));


            Type teamType = typeof(Team);
            var teamFlags = BindingFlags.Static | BindingFlags.Public;
            TeamPermissions = teamType.GetFields(teamFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));


            Type settingsType = typeof(Settings);
            var settingsFlags = BindingFlags.Static | BindingFlags.Public;
            SettingsPermissions = settingsType.GetFields(settingsFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));
        }

        public static class Task
        {
            public const string CreateTask = "ManagerPermissions.Task.CreateTask";
            public const string CreateUnassignedTask = "ManagerPermissions.Task.CreateUnassignedTask";
            public const string UpdateTask = "ManagerPermissions.Task.UpdateTask";
            public const string DeleteTask = "ManagerPermissions.Task.DeleteTask";

            public const string ChangeTaskStatus = "ManagerPermissions.Task.ChangeTaskStatus";
            //public const string UpdateCustomField = "ManagerPermissions.Task.UpdateCustomField";
            public const string ReadUnassignedTask = "ManagerPermissions.Task.ReadUnassignedTask";

        }

        public static class Agent
        {
            public const string CreateAgent = "ManagerPermissions.Agent.CreateAgent";
            public const string UpdateAgent = "ManagerPermissions.Agent.UpdateAgent";
            public const string DeleteAgent = "ManagerPermissions.Agent.DeleteAgent";
            public const string DeleteAllAgent = "ManagerPermissions.Agent.DeleteAllAgent";
            public const string UpdateAllAgent = "ManagerPermissions.Agent.UpdateAllAgent";

            public const string ViewUnverifiedAgent = "ManagerPermissions.Agent.ViewUnverifiedAgent";
            public const string ChangeAgentPassword = "ManagerPermissions.Agent.ChangeAgentPassword";
            public const string ViewDriversLoginRequests = "ManagerPermissions.Agent.ViewDriversLoginRequests";

        }
        public static class Customer
        {
            public const string CreateCustomer = "ManagerPermissions.Customer.CreateCustomer";
            public const string DeleteCustomer = "ManagerPermissions.Customer.DeleteCustomer";
            public const string UpdateCustomer = "ManagerPermissions.Customer.UpdateCustomer";
            public const string ReadCustomer = "ManagerPermissions.Customer.ReadCustomer";

        }
        public static class Team
        {
            public const string CreateTeam = "ManagerPermissions.Team.CreateTeam";
            public const string DeleteTeam = "ManagerPermissions.Team.DeleteTeam";
            public const string UpdateTeam = "ManagerPermissions.Team.UpdateTeam";
            public const string ReadTeam = "ManagerPermissions.Team.ReadTeam";
            public const string DeleteAllTeam = "ManagerPermissions.Team.DeleteAllTeam";
            public const string UpdateAllTeam = "ManagerPermissions.Team.UpdateAllTeam";
        }
        public static class Settings
        {
            public const string ReadAdvancePreference = "ManagerPermissions.Settings.ReadAdvancePreference";
            public const string UpdateAdvancedPreference = "ManagerPermissions.Settings.UpdateAdvancedPreference";
            public const string ReadAutoAllocation = "ManagerPermissions.Settings.ReadAutoAllocation";
            public const string UpdateAutoAllocation = "ManagerPermissions.Settings.UpdateAutoAllocation";
            public const string ReadGeofence = "ManagerPermissions.Settings.ReadGeofence";
            public const string UpdateGeofence = "ManagerPermissions.Settings.UpdateGeofence";
            public const string AddManager = "ManagerPermissions.Settings.AddManager";
            public const string ReadTeamManager = "ManagerPermissions.Settings.ReadTeamManager";
            public const string ReadAllManagers = "ManagerPermissions.Settings.ReadAllManagers";
            //public const string UpdateTeamManager = "ManagerPermissions.Settings.UpdateTeamManager";

            public const string UpdateAllManager = "ManagerPermissions.Settings.UpdateAllManager";
            public const string ReadNotification  = "ManagerPermissions.Settings.ReadNotification";
            public const string UpdateNotification = "ManagerPermissions.Settings.UpdateNotification";


            public const string AddGeofence = "ManagerPermissions.Settings.AddGeofence";
            public const string DeleteGeofence = "ManagerPermissions.Settings.DeleteGeofence";
            public const string UpdateTeamManager = "ManagerPermissions.Settings.UpdateTeamManager";
            public const string ChangeManagerPassword = "ManagerPermissions.Settings.ChangeManagerPassword";
            public const string AddRestaurant = "ManagerPermissions.Settings.AddRestaurant";
            public const string UpdateRestaurant = "ManagerPermissions.Settings.UpdateRestaurant";
            public const string DeleteRestaurant = "ManagerPermissions.Settings.DeleteRestaurant";
            public const string AddBranch = "ManagerPermissions.Settings.AddBranch";
            public const string UpdateBranch = "ManagerPermissions.Settings.UpdateBranch";
            public const string DeleteBranch = "ManagerPermissions.Settings.DeleteBranch";
            public const string AssignManager = "ManagerPermissions.Settings.AssignManager";
            public const string UpdateAssignManager = "ManagerPermissions.Settings.UpdateAssignManager";
            public const string DeleteAssignManager = "ManagerPermissions.Settings.DeleteAssignManager";


        }

        public static class Reports
        {
            public const string ViewReport = "ManagerPermissions.Reports.ViewReport";
            public const string ExportReport = "ManagerPermissions.Reports.ExportReport";
        }
    }
}
