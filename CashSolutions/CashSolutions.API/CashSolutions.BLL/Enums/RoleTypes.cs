﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BLL.DATA.Entities
{
    public enum RoleType : byte
    {
        Admin = 0,
        General = 1,
        Agent = 2,
        Manager = 3,
    }

    public enum DriverLoginRequestStatus: byte
    {
        Pending = 1,
        Approved,
        Rejected,
        Canceled
    }

    public enum DisplayIamge
    {
        DriverImage = 1,
        CompanyImage
    }
}
