﻿using CashSolutions.DATA.Entities;
using CashSolutions.Entities;
using CashSolutions.EntityConfigurations;
using CashSolutions.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CashSolutions.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<RolePrivilge> RolePrivilge { get; set; }
        public DbSet<Privilge> Privilge { get; set; }
        public DbSet<UserDevice> UserDevice { get; set; }
       
        public DbSet<Admin> Admins { get; set; }

        public DbSet<Machine> Machines { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new RolePrivilgeEntityTypeConfiguration());

            builder.ApplyConfiguration(new PrivilgeEntityTypeConfiguration());

            builder.ApplyConfiguration(new ApplicationRoleEntityTypeConfiguration());

            builder.ApplyConfiguration(new ApplicationRoleEntityTypeConfiguration());

         


            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
