﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.Entities;

namespace CashSolutions.DATA.Entities
{
    public class MachineStatus : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
