﻿using CashSolutions.DATA.Entities;
using CashSolutions.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.Models.Entities
{
    public class UserDevice 
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }


    }
}
