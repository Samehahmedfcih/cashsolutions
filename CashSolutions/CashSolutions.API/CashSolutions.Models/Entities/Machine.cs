﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.Entities;

namespace CashSolutions.DATA.Entities
{
    public class Machine :BaseEntity
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public long MachineId { get; set; }
        public string BranchName { get; set; }
        public string Version { get; set; }
        public string IP { get; set; }
        public long Tick { get; set; }
        public long MachineStatusId { get; set; }

       public  MachineStatus MachineStatus { set; get; }
    }
}
