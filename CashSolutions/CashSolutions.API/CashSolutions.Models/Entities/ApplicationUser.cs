﻿// Decompiled with JetBrains decompiler
// Type: CashSolutions.Entities.ApplicationUser
// Assembly: CashSolutions.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9F7AEF8A-D03C-483E-BE49-5088DBEAF76D
// Assembly location: D:\EnmaaBKp\Enmaa\Identity\CashSolutions.Models.dll

using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using CashSolutions.DATA.Entities;
using CashSolutions.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CashSolutions.Entities
{
    public class ApplicationUser : IdentityUser
    {
        private IBaseEntity baseEntity;

        public ApplicationUser(IBaseEntity _baseEntity)
        {
            this.baseEntity = _baseEntity;
        }

        public ApplicationUser()
        {}

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public bool IsAvailable { get; set; }

        //public int CountryId { set; get; }

        public string CreatedBy_Id { get; set; }

        public string UpdatedBy_Id { get; set; }

        public string DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }
        public string Tenant_Id { set; get; }

        public bool IsLocked { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }
        
        [NotMapped]
        public List<string> Permissions { get; set; }


        public List<UserDevice> UserDvices { set; get; }

    }
}
