﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using CashSolutions.Entities;

namespace CashSolutions.Models.Entities
{
    public class RolePrivilge : BaseEntity
    {
        public int Id { get; set; }

        public int FK_Privilge_Id { get; set; }

        public Privilge Privilge { get; set; }

        public string FK_Role_Id { get; set; }

        public ApplicationRole Role { get; set; }

    }
}
