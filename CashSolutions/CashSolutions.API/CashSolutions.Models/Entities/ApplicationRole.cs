﻿// Decompiled with JetBrains decompiler
// Type: CashSolutions.Entities.ApplicationRole
// Assembly: CashSolutions.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9F7AEF8A-D03C-483E-BE49-5088DBEAF76D
// Assembly location: D:\EnmaaBKp\Enmaa\Identity\CashSolutions.Models.dll

using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using CashSolutions.DATA.Entities;
using CashSolutions.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace CashSolutions.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity
    {
        private IBaseEntity baseEntity;

        public ApplicationRole()
        {
        }

        public ApplicationRole(IBaseEntity _baseEntity, string roleName)
          : base(roleName)
        {
            this.baseEntity = _baseEntity;
        }

        public string CreatedBy_Id { get; set; }

        public string UpdatedBy_Id { get; set; }

        public string DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }

        public List<Privilge> Privilges { get; set; }
        public string Tenant_Id { get; set; }
        public int Type { get; set; }
    }
}
