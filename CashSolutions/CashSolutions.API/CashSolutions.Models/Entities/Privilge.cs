﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.BuildingBlocks.BaseEntities.IEntities;
using CashSolutions.Entities;
using System.Collections.Generic;

namespace CashSolutions.Models.Entities
{
    public class Privilge : BaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ApplicationRole> Roles { get; set; }
    }
}
