﻿using CashSolutions.BuildingBlocks.BaseEntities.Entities;
using CashSolutions.Entities;

namespace CashSolutions.DATA.Entities
{
    public class Admin :BaseEntity
    {
        public string Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public string DeactivationReason { get; set; }
       // public virtual ApplicationUser User { get; set; }
    }
}
