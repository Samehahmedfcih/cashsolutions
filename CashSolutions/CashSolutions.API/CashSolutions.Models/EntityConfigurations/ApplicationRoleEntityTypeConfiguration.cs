﻿using CashSolutions.Entities;
using CashSolutions.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace CashSolutions.EntityConfigurations
{
    public class ApplicationRoleEntityTypeConfiguration
        : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> ApplicationRoleConfiguration)
        {
            ApplicationRoleConfiguration.Ignore(o => o.Privilges);
        }
    }
}
