﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AMANAH.DMS.Identity.EFCore.MSSQL.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FK_CreatedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "FK_UpdatedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "FK_CreatedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "FK_UpdatedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "FK_CreatedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "FK_UpdatedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "FK_CreatedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "FK_UpdatedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "WebsiteSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "WebsiteSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "WebsiteSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "WebsiteSettings",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "RolePrivilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "RolePrivilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "RolePrivilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "RolePrivilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "Privilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "Privilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "Privilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "Privilge",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "AspNetUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "WebsiteSettings");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "AspNetRoles");

            migrationBuilder.AddColumn<string>(
                name: "FK_CreatedBy_Id",
                table: "WebsiteSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "WebsiteSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_UpdatedBy_Id",
                table: "WebsiteSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_CreatedBy_Id",
                table: "RolePrivilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "RolePrivilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_UpdatedBy_Id",
                table: "RolePrivilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_CreatedBy_Id",
                table: "Privilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Privilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_UpdatedBy_Id",
                table: "Privilge",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_CreatedBy_Id",
                table: "AspNetRoles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "AspNetRoles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_UpdatedBy_Id",
                table: "AspNetRoles",
                nullable: true);
        }
    }
}
