﻿using CashSolutions.BLL.IManagers;
using CashSolutions.Context;
using CashSolutions.DATA.Entities;
using Autofac;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using CashSolutions.BLL.DATA.Entities;

namespace CashSolutions.API.SignalRHups
{
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class NotificationHub : Hub
    {



        public override Task OnConnectedAsync()
        {
            var user= this.Context.User;

            if (this.Context.User.IsInRole("Tenant") || this.Context.User.IsInRole("Admin") || this.Context.User.IsInRole("SuperAdmin"))
            {
                this.Groups.AddToGroupAsync(Context.ConnectionId, this.Context.User.Identity.Name);
                this.Groups.AddToGroupAsync(Context.ConnectionId, this.Context.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value);

            }
            if (Context.User.Claims.Where(x => x.Type == "UserType").FirstOrDefault().Value.ToInt32() == (int)RoleType.Manager)
            {
                this.Groups.AddToGroupAsync(Context.ConnectionId, this.Context.User.Identity.Name);
                //this.Groups.AddToGroupAsync(Context.ConnectionId, this.Context.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value);

            }

            return base.OnConnectedAsync();
        }

    }
}
