﻿using CashSolutions.Context;
using CashSolutions.DATA.Entities;
using Autofac;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CashSolutions.API.SignalRHups
{
    public class DriverHub : Hub
    {
        private readonly ILifetimeScope _hubLifetimeScope;

        private readonly ApplicationDbContext context;

        public DriverHub(ILifetimeScope lifetimeScope)
        {
            _hubLifetimeScope = lifetimeScope;

            context = _hubLifetimeScope.Resolve<ApplicationDbContext>();
        }





   

    }
}
