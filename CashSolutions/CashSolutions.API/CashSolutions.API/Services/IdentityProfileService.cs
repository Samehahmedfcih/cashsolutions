﻿using IdentityServer4.Services;
using System;
using System.Threading.Tasks;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Identity;
using IdentityServer4.Extensions;
using System.Linq;
using CashSolutions.Entities;
using System.Security.Claims;
using CashSolutions.DATA.Entities;
using CashSolutions.BLL.DATA.Entities;

namespace IdentityAuthority.Configs
{
    public class IdentityProfileService : IProfileService
    {

        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _identityRoleManager;

        public IdentityProfileService(IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> identityRoleManager)
        {
            _claimsFactory = claimsFactory;
            _userManager = userManager;
            _identityRoleManager = identityRoleManager;

        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            if (user == null)
            {
                throw new ArgumentException("");
            }

            var principal = await _claimsFactory.CreateAsync(user);
            var claims = principal.Claims.ToList();
            claims.Add(new System.Security.Claims.Claim("Tenant_Id", user.Tenant_Id));
            //Add more claims like this


            int _userType = 0;
            var UserRoles = await _userManager.GetRolesAsync(user);
            if (UserRoles != null && UserRoles.Any())
            {
                if (UserRoles.Contains("Tenant") || UserRoles.Contains("Tenant") || UserRoles.Contains("Tenant"))
                {
                    _userType = (int)RoleType.Admin;
                }
                else
                {
                    var userRoleName = UserRoles.FirstOrDefault();

                    var UserRole = await _identityRoleManager.FindByNameAsync(userRoleName);

                    if (UserRole.Type == (int)RoleType.Manager)
                    {
                        _userType = (int)RoleType.Manager;
                    }
                    else if (UserRole.Type == (int)RoleType.Agent)
                    {
                        _userType = (int)RoleType.Agent;
                    }
                }
            }

            claims.Add(new Claim("UserType", _userType.ToString()));



            //claims.Add(new System.Security.Claims.Claim("MyProfileID", user.Id));

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }

}