﻿using System.Threading.Tasks;

namespace CashSolutions.API.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
