﻿using System.Threading.Tasks;

namespace CashSolutions.API.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
