﻿using AutoMapper;

using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Microsoft.Extensions.Options;
using ML.BLL.ViewModel;
using CashSolutions;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.ViewModels;
using CashSolutions.Entities;
using CashSolutions.Settings;
using CashSolutions.ViewModels;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites;
using Utilities.Utilites.Paging;
using CashSolutions.ViewModel;
using CashSolutions.BLL.ViewModel;
using CashSolutions.DATA.Entities;
using CashSolutions.BLL.ViewModels.Admin;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Office.Interop.Excel;

namespace CashSolutions.API.Controllers
{
    [Route("api/User")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly IHostingEnvironment _hostingEnv;
        public readonly IMapper _mapper;
        public readonly UserManager<ApplicationUser> _userManager;
        IApplicationUserManager _appUserManager;
        AppSettings _appSettings;
        private readonly IUserDeviceManager _userDeviceManager;

        public UserController(IMapper mapper, UserManager<ApplicationUser> userManager,
            IApplicationUserManager appuserManager, IOptions<AppSettings> appSettings,
            IHostingEnvironment hostingEnv, IUserDeviceManager userDeviceManager
            )
        {
            _hostingEnv = hostingEnv;
            _mapper = mapper;
            _userManager = userManager;
            _appUserManager = appuserManager;
            _appSettings = appSettings.Value;
            _userDeviceManager = userDeviceManager;
        }

        #region DefaultCrudOperation
        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = _mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            entityResult = await _appUserManager.GetAsync(entityResult);
            result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }

        [Route("IsAllowToDeleteAdmin")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> IsAllowToDeleteAdmin()
        {
            try
            {
                if (await _appUserManager.IsAllowToDeleteAdmin())
                {
                    return Ok(true);
                }
                else
                {
                    return BadRequest("We Can't Delete This Admin He is the only admin on the system, you must have 1  admin on the system. ");
                }

            }
            catch (Exception e)
            {
                return BadRequest("UnExpected Error occuerd ");
            }
        }

        [Route("GetAll")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetAll()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetAll();
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }




        //[Route("GetAllPagginated")]
        //[HttpPost]
        //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        //public async Task<IActionResult> GetAllPaginated([FromBody]PaginatedItemsViewModel pagingparametermodel)
        //{
        //    PagedResult<ApplicationUserViewModel> result = new PagedResult<ApplicationUserViewModel>();
        //    PagedResult<ApplicationUser> entityResult = new PagedResult<ApplicationUser>();
        //    entityResult = await appUserManager.GetAllPagginated(pagingparametermodel);
        //    result = mapper.Map<PagedResult<ApplicationUser>, PagedResult<ApplicationUserViewModel>>(entityResult);
        //    return Ok(result);
        //}


        [Route("GetByUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public List<ApplicationUserViewModel> GetByUserIds([FromBody] List<string> userIds)
        {
            List<ApplicationUser> entityResult = _appUserManager.GetByUserIds(userIds);
            List<ApplicationUserViewModel> result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

   
        [HttpGet]
        [Route("UserRoles/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ApplicationUserViewModel> UserRoles([FromRoute] string username, string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrWhiteSpace(username))
            {
                var user = await _appUserManager.GetByEmailAsync(username);
                if (user != null)
                {
                    user.RoleNames = (await _appUserManager.GetRolesAsync(username)).ToList();
                    result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    return result;
                }

            }
            return result;
        }


        #region PostApi
        [HttpPost]
        [Route("Add")]
        //public async Task<IActionResult> Add([FromBody] ApplicationUserViewModel model)
        //{
        //    try
        //    {
        //        if (await appUserManager.IsUserNameExistAsync(model.UserName, model.Id))
        //        {
        //            return BadRequest("This User Name : " + model.UserName + " already exist ");
        //        }
        //        if (await appUserManager.IsPhoneExist(model.PhoneNumber))
        //        {
        //            return BadRequest("This Phone  : " + model.PhoneNumber + " already exist ");
        //        }



        //        IActionResult result = BadRequest("");
        //        if (ModelState.IsValid)
        //        {
        //            var entityResult = new ApplicationUser();
        //            entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(model);
        //            entityResult = (App)await appUserManager.AddUserAsync(entityResult, model.Password);
        //            model = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
        //            if (model != null)
        //            {
        //                result = Ok(model);
        //            }
        //        }
        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        return BadRequest("Sorry it's  Unexpected Error");
        //    }
        //}
        #endregion


        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        //public async Task<IActionResult> Update([FromBody] EditApplicationUserViewModel model)
        //{
        //    if (await _appUserManager.IsUserNameExistAsync(model.UserName, model.Id))
        //    {
        //        return BadRequest("This User Name : " + model.UserName + " already exist ");
        //    }

        //    bool result = false;
        //    if (ModelState.IsValid)
        //    {
        //        var entityResult = _mapper.Map<EditApplicationUserViewModel, ApplicationUser>(model);
        //        result = await _appUserManager.UpdateUserAsync(entityResult);
        //    }
        //    return Ok(result);
        //}

        #region Update User Password



        private async Task<bool> UpdateUserPassword(string userId, string newPassword, string modifiedByUserId)
        {
            ApplicationUser applicationUser = await _appUserManager.GetByUserIdAsync(userId);

            if (applicationUser == null)
            {
                return false;
            }

            applicationUser.PasswordHash = _appUserManager.HashPassword(applicationUser, newPassword);
            applicationUser.UpdatedBy_Id = modifiedByUserId;
            applicationUser.UpdatedDate = DateTime.UtcNow;

            IdentityResult identityResult = await _appUserManager.UpdateAsync(applicationUser);

            return identityResult.Succeeded;
        }

        #endregion Update User Password

        #region DeleteApi

        [Route("Delete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string username)
        {
            bool result = false;
            var entity = await _appUserManager.GetByEmailAsync(username);

            entity.UserName = entity.UserName + "_" + "Deleted_" + DateTime.UtcNow.ToString("yy_MM_dd_mm_ss");
            entity.IsDeleted = true;
            entity.DeletedDate = DateTime.UtcNow;

            var identity = await _appUserManager.UpdateAsync(entity);
            result = identity.Succeeded;
            return Ok(result);
        }



        [Route("ToggleDelete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Lock([FromRoute] string username)
        {
            bool result = false;
            var entity = await _appUserManager.GetByEmailAsync(username);
            entity.IsLocked = !entity.IsLocked;
            entity.UpdatedDate = DateTime.UtcNow;

            var identity = await _appUserManager.UpdateAsync(entity);
            result = identity.Succeeded;
            return Ok(result);
        }


        //[Route("Delete/{username}")]
        //[HttpDelete]
        //public async Task<IActionResult> Delete([FromRoute]string username)
        //{
        //    bool result = false;
        //    var entity = await appUserManager.GetByUserNameAsync(username);
        //    entity.IsDeleted = true;
        //    entity.DeletedDate = DateTime.UtcNow;

        //    var identity = await appUserManager.UpdateAsync(entity);
        //    result = identity.Succeeded;
        //    return Ok(result);
        //}

        #endregion
        #endregion

        [HttpGet]
        [Route("UsernameExists/{username}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]

        public async Task<IActionResult> UsernameExists([FromRoute] string username)
        {
            bool result = await _appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists/{email}")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> EmailExists([FromRoute] string email)
        {
            bool result = await _appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpPost]
        [Route("AssignUserToRole")]
        [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        public async Task<IActionResult> AssignUserToRole([FromBody] AssignRoleViewModel assignRoleViewModel)
        {

            bool result = await _appUserManager.AddUserToRoleAsync(assignRoleViewModel.UserName, assignRoleViewModel.RoleName);
            return Ok(result);
        }

        public void LogResult(string resultMessage, string excelFileName)
        {
            string date = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            //string dir = @"D:\Amanah\Dev\Migration"; 
            string dir = $@"{_hostingEnv.WebRootPath}\Logs";

            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string path = dir + @"\" + "Request_Log_" + excelFileName + ".txt";
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(resultMessage);
                }
            }
        }




        [HttpDelete, Route("DeleteDevice")]
        public IActionResult DeleteDevice([FromQuery] string token)
        {
            var result = _userDeviceManager.DeleteDevice(token);
            return Ok(result);
        }

        //[HttpPost]
        //[Route("ChangePassword")]
        //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
        //public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordViewModel changePasswordViewModel)
        //{
        //    var validator = new ChangePasswordViewModelValidator();
        //    var validationResult = await validator.ValidateAsync(changePasswordViewModel);
        //    if (validationResult.IsValid == false)
        //    {
        //        return BadRequest(validationResult.Errors);
        //    }

        //    if (changePasswordViewModel.oldPassword == changePasswordViewModel.newPassword)
        //        return BadRequest("enter new different Password");

        //    var user = await _appUserManager.GetByUserIdAsync(changePasswordViewModel.UserId);
        //    if (user == null)
        //        return BadRequest("User not found");

        //    bool result = await _appUserManager.ChangePasswordAsync(user, changePasswordViewModel.oldPassword, changePasswordViewModel.newPassword);


        //    return Ok(result);
        //}

     }
}