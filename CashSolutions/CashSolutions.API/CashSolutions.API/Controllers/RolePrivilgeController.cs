﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using CashSolutions.API;
using IdentityServer4.AccessTokenValidation;
using CashSolutions.BLL.IManagers;
using CashSolutions.Entities;
using CashSolutions.Models.Entities;

namespace CashSolutions.API.Controllers
{
    //[Route("api/RolePrivilge")]
    ////[Authorize(RolePrivilges = "Admin")]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    //public class RolePrivilgeController : Controller
    //{
    //    private readonly RolePrivilgeManager manger;
    //    public readonly IMapper mapper;
    //    public RolePrivilgeController(RolePrivilgeManager _manger, IMapper _mapper)
    //    {
    //        mapper = _mapper;
    //        manger = _manger;
    //    }
    
    //    #region DefaultCrudOperation

    //    #region GetApi
    //    [Route("Get")]
    //    [HttpGet]
    //    public IActionResult Get(int  id)
    //    {
    //        var entityResult =  manger.Get(id);
    //        var result = mapper.Map<RolePrivilge, RolePrivilgeViewModel>(entityResult);
    //        return Ok(result);
    //    }
    //    [Route("GetAll")]
    //    [HttpGet]
    //    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    //    public IActionResult GetAll()
    //    {
    //        var entityResult = manger.GetAll().ToList();
    //        var result = mapper.Map<List<RolePrivilge>, List<RolePrivilgeViewModel>>(entityResult);
    //        return Ok(result);
    //    }
    //    #endregion
    //    #region PostApi
    //    //string should be replced based on each Controller EntityDTO Object 
    //    [Route("Add")]
    //    [HttpPost]
    //    public IActionResult Post([FromBody]RolePrivilgeViewModel model)
    //    {
    //        var entityResult = mapper.Map<RolePrivilgeViewModel, RolePrivilge>(model);
    //        entityResult =  manger.AddAsync(entityResult);
    //        var result = mapper.Map<RolePrivilge, RolePrivilgeViewModel>(entityResult);
    //        return Ok(result);

    //    }
    //    #endregion
    //    #region PutApi
    //    //string should be replced based on each Controller EntityDTO Object 
    //    [Route("Update")]
    //    [HttpPost]
    //    public IActionResult Put([FromBody]RolePrivilgeViewModel model)
    //    {
    //        bool result = false;
    //        var entityResult = mapper.Map<RolePrivilgeViewModel, RolePrivilge>(model);
    //        result =  manger.Update(entityResult);
    //        return Ok(result);
    //    }
    //    #endregion
    //    #region DeleteApi
    //    [Route("Delete/{RolePrivilgeName}")]
    //    [HttpDelete]
    //    //[Authorize(RolePrivilges = "admin")]
    //    public IActionResult Delete([FromRoute]int id)
    //    {
    //        bool result = false;
    //        result =  manger.DeleteById(id);
    //        return Ok(result);
    //    }
    //    #endregion
    //    #endregion
    //}
}