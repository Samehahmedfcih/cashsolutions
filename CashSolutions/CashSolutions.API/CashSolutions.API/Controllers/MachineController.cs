﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace CashSolutions.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MachineController : ControllerBase
    {


        private IMachineManager _MachineManager { set; get; }

        public MachineController(IMachineManager MachineManager)
        {
            _MachineManager = MachineManager;
        }


        // GET: Machine
        [Route("GetAll")]
        [HttpGet]
        //  [AllowAnonymous]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllMachines 
            var AllMachines = await _MachineManager.GetAllAsync<MachineViewModel>(true);
            return Ok(AllMachines);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            var AllTeams = await _MachineManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


        // GET: Machine/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var Machine = await _MachineManager.Get(id);
            return Ok(Machine);
        }


        // POST: Teams/Machine
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody]MachineViewModel MachineVM)
        {
            try

            {
                    var created_Machine = await _MachineManager.AddAsync(MachineVM);
                    // TODO: Add insert logic here

                    return Ok(created_Machine);
                
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }


        // POST: Machine/Edit/5
        [HttpPut]
        public async Task<ActionResult> Update(MachineViewModel MachineVM)
        {
            try
            {
                
                    var updated_Machine = await _MachineManager.UpdateAsync(MachineVM);
                    return Ok(updated_Machine);
                
            }
            catch
            {
                return BadRequest("Un Handled Exception ");
            }
        }




        // GET: MachineToDelete/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                var MachineToDelete = await _MachineManager.Get(id);
                var result = await _MachineManager.SoftDeleteAsync(MachineToDelete);
                // TODO: Add insert logic here

                if (result)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest("Can't Delete this Machine");
                }
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }
    }
}