﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.ViewModels;
using CashSolutions.BLL.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CashSolutions.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        protected readonly IApplicationUserManager _userManager;
        public AccountController(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {

            var existUser = await _userManager.GetByEmailAsync(model.Email);
            if (existUser != null)
                return BadRequest("This email is already exist");


            var res = await _userManager.AddTenantAsync(model);

            if (res.Succeeded == false)
            {
                if(res.Errors.Count > 0) { 
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
                }
            }


            return Ok(res);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            var res = await _userManager.LoginAsync(model);
            if (res.Errors.Any())
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest model)
        {
            var res = await _userManager.ForgotPassword(model.Email);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest model)
        {
            var res = await _userManager.ResetPasswordAsync(model.Email, model.Token, model.NewPassword);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CheckResetToken([FromBody] CheckResetTokenRequest model)
        {
            var res = await _userManager.CheckResetToken(model.Email, model.Token);

            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }
    }
}