﻿using CashSolutions.BLL.Managers;
using Autofac;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CashSolutions.API
{
    public class AutoFacContainerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            //for  register all interfaces for all Mangers 
            builder.RegisterAssemblyTypes(typeof(ApplicationUserManager).Assembly).AsImplementedInterfaces();

           
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().InstancePerLifetimeScope();



        }
    }
}
