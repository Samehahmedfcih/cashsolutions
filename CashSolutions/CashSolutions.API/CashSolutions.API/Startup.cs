﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using CashSolutions.API.Certificates;
using CashSolutions.API.Services;
using CashSolutions.BLL.BLL.IManagers;
using CashSolutions.BLL.Managers;
using CashSolutions.BLL.BLL.Settings;
using CashSolutions.BLL.IManagers;
using CashSolutions.BLL.Managers;
using CashSolutions.Context;
using CashSolutions.Entities;
using CashSolutions.Models.Entities;

using CashSolutions.Settings;
using IdentityAuthority.Configs;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using System;
using System.Net;
using System.Linq;
using IdentityServer4.AccessTokenValidation;
using System.IdentityModel.Tokens.Jwt;
using CashSolutions.Repoistry;
using FluentValidation.AspNetCore;
using AutoWrapper;
using NSwag;
using NSwag.Generation.Processors.Security;
using Microsoft.AspNetCore.Authorization;
using CashSolutions.API.Authorization;
using CashSolutions.BLL.Authorization;
using Utilites.UploadFile;
using Microsoft.Extensions.FileProviders;
using System.IO;
using CashSolutions.API.SignalRHups;
using Hangfire;
using Hangfire.SqlServer;

using Newtonsoft.Json.Converters;

namespace CashSolutions.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {

                options.AddPolicy("CorsPolicy",
                  builder => builder
                  .AllowAnyMethod()
                  .AllowAnyHeader().AllowCredentials()
                  .SetIsOriginAllowed((x) => true));
                        
            });


            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
             options.UseSqlServer(Configuration["ConnectionString"],
              sqlOptions => sqlOptions.MigrationsAssembly("DispatchProduct.Identity.EFCore.MSSQL")));
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddIdentityServer();

            ConfigureAuthService(services);

            services.Configure<AppSettings>(Configuration);

            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));

            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration["ConnectionString"], new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));

            services.AddHangfireServer();

            services.AddMvc()
                .AddNewtonsoftJson(options =>
                                options.SerializerSettings.Converters.Add(new StringEnumConverter()));


            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();


            services.AddTransient<DbContext, ApplicationDbContext>();


            services.AddScoped(typeof(IAgentAccessControlManager), typeof(AgentAccessControlManager));



            services.AddScoped(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));

            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IPrivilgeManager), typeof(PrivilgeManager));

            services.AddScoped(typeof(IRolePrivilgeManager), typeof(RolePrivilgeManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(IUserDeviceManager), typeof(UserDeviceManager));

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();


            services.AddSingleton(typeof(IEmailSenderservice), typeof(EmailSenderService));
            services.AddSingleton(typeof(EmailSettings), typeof(EmailSettings));
            services.AddScoped(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            services.AddScoped(typeof(ITenantService), typeof(TenantService));
 

            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddSwaggerGen(config =>
            {
                config.DescribeAllEnumsAsStrings();
                //config.OperationFilter<SupportMultipleEnumValues>();
            });
            services.AddSwaggerDocument(config =>
               {
                   config.PostProcess = document =>
                   {
                       document.Info.Version = "v1";
                       document.Info.Title = "DMS API";
                       document.Info.Description = "";
                       document.Info.TermsOfService = "None";
                   };

                   config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                   config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                       new OpenApiSecurityScheme()
                       {
                           Type = OpenApiSecuritySchemeType.ApiKey,
                           Name = "Authorization",
                           In = OpenApiSecurityApiKeyLocation.Header,
                           Description = "Copy this into the value field: Bearer {token}"
                       }
                   );
               });

            services.AddSignalR();


            var connectionString = Configuration["ConnectionString"];
            //var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var migrationsAssembly = "DispatchProduct.Identity.EFCore.MSSQL";
            // Adds IdentityServer
            services.AddIdentityServer(x => x.IssuerUri = "null")
                .AddSigningCredential(Certificate.Get())
                .AddAspNetIdentity<ApplicationUser>()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString, opts =>
                        opts.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(connectionString, opts =>
                         opts.MigrationsAssembly(migrationsAssembly));
                })
                .Services.AddTransient<IProfileService, IdentityProfileService>();
            //.Services.AddTransient<IProfileService, ProfileService>();
            //var container = new ContainerBuilder();

            //container.RegisterModule<AutoFacContainerModule>();
            //container.Populate(services);

            //container.Populate(services);

            //return new AutofacServiceProvider(container.Build());




            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();


            services.AddAuthorization(options =>
            {

                foreach (var item in ManagerPermissions.AgentPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in ManagerPermissions.CustomerPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in ManagerPermissions.OrderPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in ManagerPermissions.TeamPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in ManagerPermissions.SettingsPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in AgentPermissions.TaskPermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }

                foreach (var item in AgentPermissions.ProfilePermissions)
                {
                    options.AddPolicy(item, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(item));
                    });
                }
            });



            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<AutoFacContainerModule>();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();

            services.AddSingleton<IConfiguration>(Configuration);
            return new AutofacServiceProvider(container);
        }

        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {

            app.UseCors("CorsPolicy");

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
                builder.AddUserSecrets<Startup>();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            builder.AddEnvironmentVariables();

            //app.UseSwaggerUi(typeof(Startup).Assembly,);
            app.UseExceptionHandler(builder =>
            {
                // Adds a terminal middleware delegate to the application
                // request pipeline
                // The Run() method can access the http request/response context
                builder.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    var error = context.Features.Get<IExceptionHandlerFeature>();

                    if (error != null)
                    {
                        // We are going to extend context.Response so that we can add our
                        // own custom error response headers

                        //context.Response.AddApplicationError(error.Error.Message);

                        await context.Response.WriteAsync(error.Error.Message);
                    }
                });
            });

            ConfigureAuth(app);

            app.UseOpenApi();
            app.UseSwaggerUi3();


            app.UseRouting();
            app.UseAuthorization();

            app.UseStaticFiles();


     





            app.UseHangfireDashboard();
            backgroundJobs.Enqueue(() => Console.WriteLine("Hello world from Hangfire!"));


            //var options = new RewriteOptions()
            // .AddRedirectToHttps();
            //ConfigureAuth(app);
            //app.UseRewriter(options);
            // app.UseAuthentication();
            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            // Adds IdentityServer
            app.UseIdentityServer();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notificationHub");
                endpoints.MapHub<DriverHub>("/driverHub");


            });
        }



        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;

                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {

            app.UseAuthentication();

        }

    }
}
