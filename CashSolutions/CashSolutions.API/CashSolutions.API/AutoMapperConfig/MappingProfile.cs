﻿using CashSolutions.BLL.Models;
using CashSolutions.BLL.ViewModels;
using CashSolutions.BLL.ViewModels.Account;
using CashSolutions.DATA.Entities;
using CashSolutions.Entities;
using CashSolutions.Models.Entities;
using CashSolutions.ViewModel;
using CashSolutions.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using Utilities.Utilites.Paging;
using Microsoft.AspNetCore.Mvc;
using CashSolutions.BLL.ViewModels.Admin;

namespace CashSolutions.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {



            CreateMap<ApplicationUser, UserViewModel>(MemberList.None)
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + ' ' + src.MiddleName + ' ' + src.LastName));
            CreateMap<IdentityResult, UserManagerResult>(MemberList.None);

            CreateMap<UserDevice, UserDeviceViewModel>(MemberList.None);
            CreateMap<UserDeviceViewModel, UserDevice>(MemberList.None);




            // Add as many of these lines as you need to map your objects
            CreateMap<RolePrivilgeViewModel, RolePrivilge>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));
            CreateMap<RolePrivilge, RolePrivilgeViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));

            CreateMap<PrivilgeViewModel, Privilge>(MemberList.None)
             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<Privilge, PrivilgeViewModel>()
              .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<ApplicationRoleViewModel, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges))
              .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges));

            CreateMap<ApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());


            CreateMap<ApplicationUser, ApplicationUserViewModel>(MemberList.None)
            .ForMember(dest => dest.Password, opt => opt.Ignore());


            CreateMap<PagedResult<ApplicationUser>, PagedResult<ApplicationUserViewModel>>(MemberList.None);
         
          CreateMap<AdminFullProfile, AdminProfileViewModel>();
            CreateMap<AdminFullProfile, UserInfoViewModel>();
            CreateMap<AdminViewModel, Admin>().ReverseMap();
        }





    }
}

