﻿using CashSolutions.BASE;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.Repoistry
{
    public interface IBaseManagerWithContext<TViewModel, TEntity> : IBaseManager<TViewModel, TEntity>
    {
        MainTaskViewModelClaimsPrincipal CustomerHttpContextUser { get;  set; }
    }
}
