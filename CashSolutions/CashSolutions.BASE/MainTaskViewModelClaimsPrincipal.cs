﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashSolutions.BASE
{
    public class MainTaskViewModelClaimsPrincipal
    {
        public string Tenant_Id { get; set; }
        public string UserName { get; set; }
    }
}
