﻿using CashSolutions.BASE;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CashSolutions.Repoistry
{
    public class BaseManagerWithContext<TViewModel, TEntity> : BaseManager<TViewModel, TEntity>, IBaseManagerWithContext<TViewModel, TEntity>
    {
        private readonly ITenantService _tenantService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public BaseManagerWithContext(DbContext context, IRepositry<TEntity> repository, IMapper mapper, IHttpContextAccessor httpContextAccessor, ITenantService tenantService)
            : base(context, repository, mapper)
        {
            _httpContextAccessor = httpContextAccessor;
            _tenantService = tenantService;
        }
        public BaseManagerWithContext(DbContext context, IRepositry<TEntity> repository, IMapper mapper, ITenantService tenantService)
            : base(context, repository, mapper)
        {
            _tenantService = tenantService;
        }
        public BaseManagerWithContext(DbContext context, IRepositry<TEntity> repository, IMapper mapper, IHttpContextAccessor httpContextAccessor)
            : base(context, repository, mapper)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public BaseManagerWithContext(DbContext context, IRepositry<TEntity> repository, IMapper mapper)
            : base(context, repository, mapper)
        {

        }
        
        private MainTaskViewModelClaimsPrincipal customerHttpContextUser;
        public MainTaskViewModelClaimsPrincipal CustomerHttpContextUser
        {
            get => customerHttpContextUser;
            set
            {
                TenantId = value.Tenant_Id;
                UserName = value.UserName;
            }
        }

        protected string TenantId;
        protected string UserName;

        protected string GetCurrentUser_Name()
        {
            if (!string.IsNullOrWhiteSpace(UserName))
                return TenantId;

            return _httpContextAccessor?.HttpContext.User.Identity.Name;
        }

        protected string GetCurrentTenant_Id()
        {
            if (!string.IsNullOrWhiteSpace(TenantId))
                return TenantId;

            if (_tenantService != null)
            return _tenantService.GetCurrentTenant_Id();

            if (_httpContextAccessor.HttpContext.User != null)
            {
                if (_httpContextAccessor.HttpContext.User.Claims.Count() > 0)
                {
                    return _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value;
                }
            }
            return null;
        }
    }
}
