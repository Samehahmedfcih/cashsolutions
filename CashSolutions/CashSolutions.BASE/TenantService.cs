﻿

using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace CashSolutions.Repoistry
{
    public class TenantService : ITenantService
    {
        private readonly HttpContext _httpContext;

        public TenantService(IHttpContextAccessor accessor)
        {
            _httpContext = accessor.HttpContext;

        }

        /// <summary>
        /// Get Current Tenenat Id
        /// </summary>
        /// <returns></returns>
        public string GetCurrentTenant_Id()
        {
            if (_httpContext == null)
                return null;

            if (_httpContext.User == null)
                return null;

            if (_httpContext.User.Claims.Count() > 0)
                return _httpContext.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value;

            return null;
        }

    }


}
